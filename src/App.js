import ActionCable from 'actioncable'
import React, { Component } from 'react';

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      app: ''
    }
  }
  
  cable = () => {
    var cable = ActionCable.createConsumer('ws://192.168.88.216:3000/cable')
    let app = cable.subscriptions.create('RoutingChannel',{
        connected: function() {
          console.log("connected to web socket! Looking at data:");
        },

        create: function(data) {
          this.perform('send_message', {
           data
          })
        },

        received: function(data) {
          console.log("DATA", data)
        }
      })
   
    this.setState({
      app
    })  
  }

  componentDidMount() {
    this.cable()
  }

  onPress = () => {
    let data = {
      job_id: 'J2b123',
      lat: 13.34,
      long: -30.0,
      driver_id: 4
    }
    let res = this.state.app.create(data)
    console.log("RESP", res)
  }

  render() {
    return(
      <div>
        <button onClick={this.onPress}></button>
      </div>
    )
  }
}

export default App;
